from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import time


webdriver_path = "/Users/amir/Downloads/chromedriver"
driver = webdriver.Chrome(webdriver_path)

# defining target page virable for ease of use  
target_website_url = "https://www.instagram.com/exitomag/"

# opening the target instagram page
driver.get(target_website_url)    

# targeting username and password input field
username = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='username']")))
password = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='password']")))

# username and password of instagram account
username.clear()
username.send_keys("test_account_exa")
password.clear()
password.send_keys("thisisatest")

# clicking log in button
button = WebDriverWait(driver, 2).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "button[type='submit']"))).click()

# sleep for page to be load
time.sleep(2)

# clicking Not Now button to reach the target page
not_now_alert = WebDriverWait(driver, 15).until(EC.element_to_be_clickable((By.XPATH, '//button[contains(text(), "Not Now")]'))).click()